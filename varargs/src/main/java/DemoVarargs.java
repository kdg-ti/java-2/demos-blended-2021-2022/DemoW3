/**
 * Mark Goovaerts
 * 4/10/2021
 */
public class DemoVarargs {
    public static void main(String[] args) {
        System.out.println("resultaat van sommeer: " + sommeer(1, 2));
    }

    private static int sommeer(int getal1, int getal2) {
        return getal1 + getal2;
    }
}
